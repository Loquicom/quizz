import {WebSocketHandler} from './web-socket-handler';

export class WebSocketPlayerHandler extends WebSocketHandler {

  handler(resource: string, data: any): void {
    switch (resource) {
      case 'question':
        this.question(data);
        break;
      case 'wait':
        this.wait();
        break;
      case 'score':
        this.score(data);
        break;
      case 'end':
        this.end();
        break;
    }
  }

  private question(data: any): void {
    this.dataService.data = data;
    this.navigate('/question');
  }

  private wait(): void {
    this.navigate('/wait');
  }

  private score(data: any[]): void {
    data.forEach(elt => {
      this.dataService.getPlayer(elt.pseudo).score = elt.score;
    });
  }

  private end(): void {
    this.navigate('/final');
  };

}
