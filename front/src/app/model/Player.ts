export class Player {

  public score: number = 0;

  public answer: string = '';

  constructor(private pseudo: string) {
  }

  public getPseudo(): string {
    return this.pseudo;
  }

}
