import {WebSocketHandler} from './web-socket-handler';

export class WebSocketGameMasterHandler extends WebSocketHandler {

  handler(resource: string, data: any): void {
    switch (resource) {
      case 'result':
        this.resuslt(data);
        break;
    }
  }

  private resuslt(data: any): void {
    const player = this.dataService.getPlayer(data.pseudo);
    player.answer = data.answer;
  };

}
