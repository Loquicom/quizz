import {DataService} from '../service/data/data.service';
import {Router} from '@angular/router';

export abstract class WebSocketHandler {

  protected action;

  constructor(
    protected router: Router,
    protected dataService: DataService
  ) {
  }

  public abstract handler(resource: string, data: any): void;

  protected navigate(route: string): void {
    this.router.navigate([route]);
  }

}
