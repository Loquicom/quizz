import {Component, OnInit} from '@angular/core';
import {WebSocketService} from './service/web-socket/web-socket.service';
import {Router} from '@angular/router';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {

  constructor(
    public ws: WebSocketService,
    private router: Router
  ) {
  }

  ngOnInit(): void {
    if (!this.ws.isConnected()) {
      this.router.navigate(['/login']);
    }
  }

}
