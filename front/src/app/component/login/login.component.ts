import {Component, OnInit, ViewChild} from '@angular/core';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {WebSocketService} from '../../service/web-socket/web-socket.service';
import {DataService} from '../../service/data/data.service';
import {LoaderComponent} from '../loader/loader.component';
import {WebSocketGameMasterHandler} from '../../model/ws-game-master-handler';
import {WebSocketPlayerHandler} from '../../model/ws-player-handler';
import {Router} from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  @ViewChild('loader') loader: LoaderComponent;

  public form: FormGroup;

  public error = {
    show: false, message: '', click: () => {
      this.error.show = false;
    }
  };

  constructor(
    private ws: WebSocketService,
    private data: DataService,
    private router: Router
  ) {
  }

  ngOnInit() {
    //Si deja connecté on déconnecte
    if (this.ws.isConnected()) {
      this.ws.disconnect();
    }
    //Création formulaire
    this.form = new FormGroup({
      'pseudo': new FormControl('', [
        Validators.required
      ]),
      'server': new FormControl('', [
        Validators.required
      ])
    });
  }

  public isValid(): boolean {
    return this.form.valid;
  }

  public join(): void {
    //Verif que le formulaire est bien valide
    if (!this.isValid()) {
      return;
    }
    //Cache erreur et affiche loader
    this.error.show = false;
    this.loader.show();
    //Tentative connexion websocket
    const obs = this.ws.connect(this.form.value.server, true);
    obs.subscribe((result) => {
      //Reussite
      if (result) {
        //Recupere info utilisateur sur le serveur
        this.ws.getInfo().subscribe((result) => {
          this.data.setBaseValue(result.id, result.gm, this.form.value.pseudo);
          this.loader.hide();
          if (this.data.isGm()) {
            this.ws.setHandler(new WebSocketGameMasterHandler(this.router, this.data), true);
            this.router.navigate(['/create']);
          } else {
            this.ws.setHandler(new WebSocketPlayerHandler(this.router, this.data), true);
            this.router.navigate(['/wait']);
          }
        }, (error) => {
          this.error.message = error;
          this.error.show = true;
          this.loader.hide();
        });
      }
      //Echec
      else {
        this.error.message = 'Unable to connect to the server';
        this.error.show = true;
        this.loader.hide();
      }
    });
  }

}
