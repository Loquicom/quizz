import {Component, OnInit} from '@angular/core';
import {DataService} from '../../service/data/data.service';
import {Player} from '../../model/Player';
import {WebSocketService} from '../../service/web-socket/web-socket.service';

@Component({
  selector: 'app-final',
  templateUrl: './final.component.html',
  styleUrls: ['./final.component.css']
})
export class FinalComponent implements OnInit {

  public winners: Player[];

  constructor(
    public dataService: DataService,
    private ws: WebSocketService
  ) {
  }

  ngOnInit() {
    this.winners = this.dataService.getWinners();
    setTimeout(() => this.ws.disconnect(), 1000);
  }

}
