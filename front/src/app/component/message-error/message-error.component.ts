import {Component, Input, OnInit} from '@angular/core';

@Component({
  selector: 'app-message-error',
  templateUrl: './message-error.component.html',
  styleUrls: ['./message-error.component.css']
})
export class MessageErrorComponent implements OnInit {

  @Input() message: string;

  @Input() click: Function;

  @Input() messageClass = '';

  constructor() {
  }

  ngOnInit() {
    if (this.click === undefined) {
      this.click = this.hideMessage;
    }
  }

  public hideMessage() {
    this.messageClass += ' hide';
  }

}
