import {Component, OnInit} from '@angular/core';
import {DataService} from '../../service/data/data.service';
import {WebSocketService} from '../../service/web-socket/web-socket.service';
import {Router} from '@angular/router';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {

  constructor(
    public data: DataService,
    private ws: WebSocketService,
    private router: Router
  ) {
  }

  ngOnInit() {
  }

  public logout(): void {
    this.ws.disconnect();
    this.router.navigate(['/login']);
  }

}
