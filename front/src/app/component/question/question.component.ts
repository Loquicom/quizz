import {Component, OnInit} from '@angular/core';
import {DataService} from '../../service/data/data.service';
import {WebSocketService} from '../../service/web-socket/web-socket.service';
import {Router} from '@angular/router';

@Component({
  selector: 'app-question',
  templateUrl: './question.component.html',
  styleUrls: ['./question.component.css']
})
export class QuestionComponent implements OnInit {

  public answer = new Array(6);

  constructor(
    private ws: WebSocketService,
    public dataService: DataService,
    private router: Router
  ) {
  }

  ngOnInit() {
  }

  public isValid() {
    if (this.dataService.data.type === 'qcrm') {
      let result = false;
      this.answer.forEach(elt => {
        if (elt !== undefined) {
          result = result || elt;
        }
      });
      return result;
    } else {
      return this.answer[0] !== undefined && this.answer[0].trim() !== '';
    }
  }

  public send(): void {
    if (!this.isValid()) {
      return;
    }
    this.ws.send('to_gm', {resource: 'result', gm: this.dataService.data.gm, data: this.getDataFromForm()});
    this.router.navigate(['/wait']);
  }

  private getDataFromForm() {
    let answer = '';
    if (this.dataService.data.type === 'qcrm') {
      this.answer.forEach((elt, index) => {
        if (elt !== undefined && elt) {
          answer += this.dataService.data.answers[index] + ', ';
        }
      });
    } else {
      this.answer.forEach(elt => {
        if (elt !== undefined && elt.trim() !== '') {
          answer += elt + ' ';
        }
      });
    }
    return {
      pseudo: this.dataService.getPseudo(),
      answer: answer.trim().replace(/,+$/, '')
    };
  }

}
