import {Component, OnInit} from '@angular/core';
import {DataService} from '../../service/data/data.service';

@Component({
  selector: 'app-wait',
  templateUrl: './wait.component.html',
  styleUrls: ['./wait.component.css']
})
export class WaitComponent implements OnInit {

  constructor(
    public dataService: DataService
  ) {
  }

  ngOnInit() {
  }

}
