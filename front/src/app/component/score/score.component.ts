import {Component, OnInit} from '@angular/core';
import {DataService} from '../../service/data/data.service';
import {WebSocketService} from '../../service/web-socket/web-socket.service';
import {Router} from '@angular/router';

@Component({
  selector: 'app-score',
  templateUrl: './score.component.html',
  styleUrls: ['./score.component.css']
})
export class ScoreComponent implements OnInit {

  constructor(
    public dataService: DataService,
    private ws: WebSocketService,
    private router: Router
  ) {
  }

  ngOnInit() {
  }

  public validate(): void {
    this.ws.send('to_other', {resource: 'score', data: this.dataService.players});
    this.router.navigate(['/create']);
  }

  public end(): void {
    this.ws.send('to_other', {resource: 'score', data: this.dataService.players});
    this.ws.send('to_other', {resource: 'end', data: null});
    this.router.navigate(['/final']);
  }

}
