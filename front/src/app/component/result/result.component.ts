import {Component, OnInit} from '@angular/core';
import {DataService} from '../../service/data/data.service';
import {WebSocketService} from '../../service/web-socket/web-socket.service';
import {Router} from '@angular/router';

@Component({
  selector: 'app-result',
  templateUrl: './result.component.html',
  styleUrls: ['./result.component.css']
})
export class ResultComponent implements OnInit {

  constructor(
    public dataService: DataService,
    private ws: WebSocketService,
    private router: Router
  ) {
  }

  ngOnInit() {
  }

  public stop(): void {
    this.ws.send('to_other', {resource: 'wait', data: null});
    this.router.navigate(['/score']);
  }

}
