import {Component, OnInit, ViewChild} from '@angular/core';
import {WebSocketService} from '../../service/web-socket/web-socket.service';
import {LoaderComponent} from '../loader/loader.component';
import {DataService} from '../../service/data/data.service';
import {Router} from '@angular/router';

@Component({
  selector: 'app-create',
  templateUrl: './create.component.html',
  styleUrls: ['./create.component.css']
})
export class CreateComponent implements OnInit {

  @ViewChild('loader') loader: LoaderComponent;

  public question: string;

  public preview: string;

  public type: string;

  public answers: string[] = new Array<string>(6);

  constructor(
    private ws: WebSocketService,
    private data: DataService,
    private router: Router
  ) {
  }

  ngOnInit() {
  }

  public changeType(): void {
    this.answers = new Array<string>(6);
  }

  public isValid(): boolean {
    if (this.question === undefined || this.question.trim() === '') {
      return false;
    }
    if (this.type === undefined) {
      return false;
    }
    if (this.answers === undefined) {
      return false;
    }
    if (this.type !== 'text' && (this.answers[0] === undefined || this.answers[0].trim() === '')) {
      return false;
    }
    return true;
  }

  public markdown(): void {
    this.loader.show();
    const result = this.ws.receive();
    this.ws.send('markdown', this.question);
    result.subscribe((result) => {
      this.loader.hide();
      this.preview = result.md;
    }, (error => {
      this.loader.hide();
    }));
  }

  public send(): void {
    if (!this.isValid()) {
      return;
    }
    this.data.clearAnswers();
    this.ws.send('to_other', {resource: 'question', data: this.getDataFromForm()});
    this.router.navigate(['/result']);
  }

  private getDataFromForm() {
    const answers = [];
    this.answers.forEach((elt) => {
      if (elt !== undefined && elt.trim() !== '') {
        answers.push(elt);
      }
    });
    return {
      gm: this.data.getId(),
      question: this.preview,
      type: this.type,
      answers: answers
    };
  }

}
