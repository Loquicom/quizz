import {Component, Input, OnInit, ViewChild} from '@angular/core';
import {MzModalComponent} from 'ngx-materialize';

@Component({
  selector: 'app-loader',
  templateUrl: './loader.component.html',
  styleUrls: ['./loader.component.css']
})
export class LoaderComponent implements OnInit {

  @Input() text: string = 'Loading...';

  @Input() modal: boolean = true;


  @ViewChild('loaderModal') modalComponent: MzModalComponent;

  public modalOptions = {
    dismissible: false,
    startingTop: '100%',
    endingTop: '10%'
  };

  constructor() {
  }

  ngOnInit() {
  }

  public show(): void {
    this.modalComponent.openModal();
  }

  public hide(): void {
    this.modalComponent.closeModal();
  }

}
