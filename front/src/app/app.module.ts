import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';

import {AppComponent} from './app.component';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {AppRoutingModule} from './app-routing.module';
import {LoginComponent} from './component/login/login.component';
import {
  MzButtonModule,
  MzCheckboxModule,
  MzIconMdiModule,
  MzInputModule,
  MzModalModule,
  MzRadioButtonModule,
  MzSelectModule,
  MzTextareaModule,
  MzTooltipModule
} from 'ngx-materialize';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {WebSocketService} from './service/web-socket/web-socket.service';
import {HeaderComponent} from './component/header/header.component';
import {MessageErrorComponent} from './component/message-error/message-error.component';
import {LoaderComponent} from './component/loader/loader.component';
import {CreateComponent} from './component/create/create.component';
import {WaitComponent} from './component/wait/wait.component';
import {QuestionComponent} from './component/question/question.component';
import { ResultComponent } from './component/result/result.component';
import { ScoreComponent } from './component/score/score.component';
import { FinalComponent } from './component/final/final.component';

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    HeaderComponent,
    MessageErrorComponent,
    LoaderComponent,
    CreateComponent,
    WaitComponent,
    QuestionComponent,
    ResultComponent,
    ScoreComponent,
    FinalComponent
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    AppRoutingModule,
    MzInputModule,
    MzIconMdiModule,
    MzButtonModule,
    ReactiveFormsModule,
    MzTooltipModule,
    MzModalModule,
    MzTextareaModule,
    FormsModule,
    MzSelectModule,
    MzRadioButtonModule,
    MzCheckboxModule
  ],
  providers: [WebSocketService],
  bootstrap: [AppComponent]
})
export class AppModule {
}
