import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {LoginComponent} from './component/login/login.component';
import {CreateComponent} from './component/create/create.component';
import {WaitComponent} from './component/wait/wait.component';
import {QuestionComponent} from './component/question/question.component';
import {ResultComponent} from './component/result/result.component';
import {ScoreComponent} from './component/score/score.component';
import {FinalComponent} from './component/final/final.component';

const routes: Routes = [
  {path: '', redirectTo: '/login', pathMatch: 'full'},
  {path: 'login', component: LoginComponent},
  {path: 'create', component: CreateComponent},
  {path: 'wait', component: WaitComponent},
  {path: 'question', component: QuestionComponent},
  {path: 'result', component: ResultComponent},
  {path: 'score', component: ScoreComponent},
  {path: 'final', component: FinalComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {
}
