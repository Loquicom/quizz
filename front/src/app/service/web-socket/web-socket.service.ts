import {Injectable} from '@angular/core';
import {Observable} from 'rxjs';
import {WebSocketHandler} from '../../model/web-socket-handler';

@Injectable({
  providedIn: 'root'
})
export class WebSocketService {

  /**
   * Indique si la WebSocket est connecté
   */
  private connected = false;

  /**
   * La WebSocket
   */
  private socket: WebSocket;

  /**
   * L'instance avec les infos pour repondre aux actions du serveur
   */
  private handler: WebSocketHandler;

  /**
   * Utilise ou non le handler
   */
  private useHandler: boolean = false;

  /**
   * Les données envoyé par la WebSocket
   */
  private data: any;

  constructor() {
  }

  /**
   * Connexion à un serveur
   * @param host
   * @param debug
   */
  public connect(host: string, debug = false): Observable<boolean> {
    this.disconnect();
    this.socket = new WebSocket('ws://' + host);
    return new Observable<boolean>(observer => {
      this.socket.onopen = () => {
        this.connected = true;
        this.defaultAction(debug);
        observer.next(true);
      };
      this.socket.onerror = function () {
        observer.next(false);
      };
    });
  }

  /**
   * Déconnexion
   */
  public disconnect(): void {
    if (this.socket === undefined) {
      return;
    }
    this.socket.close();
    this.socket = undefined;
    this.connected = false;
  }

  public send(action: string, data: any = null): void {
    const json = JSON.stringify({action: action, data: data});
    this.socket.send(json);
  }

  /**
   * Indique que l'on attend la reception de donnée par le serveur
   * (A utiliser avant send pour etre sur d'avoir les données)
   */
  public receive(): Observable<any> {
    return new Observable(observer => {
      this.socket.onmessage = (event) => {
        if (this.useHandler) {
          this.enableHandler();
        } else {
          this.defaultAction();
        }
        const result = JSON.parse(event.data);
        this.data = result.data;
        if (result.status === 'ok') {
          observer.next(this.data);
        } else {
          observer.error(this.data);
        }
      };
    });
  }

  /**
   * Recupère les infos de l'utilisateur sur le serveur
   */
  public getInfo(): Observable<any> {
    const result = this.receive();
    this.send('info');
    return result;
  }

  /**
   * La socket est elle connecté ?
   */
  public isConnected(): boolean {
    return this.connected;
  }

  /**
   * Recupère les données
   */
  public getData(): any {
    return this.data;
  }

  /**
   * Définit le handler à utiliser
   * @param handler
   * @param enable Active immédiatement
   */
  public setHandler(handler: WebSocketHandler, enable = false) {
    this.handler = handler;
    if (enable) {
      this.enableHandler();
    }
  }

  /**
   * Active le handler
   */
  public enableHandler(): void {
    if (!(this.handler !== undefined && this.handler !== null)) {
      return;
    }
    this.useHandler = true;
    this.socket.onmessage = (event) => {
      this.data = JSON.parse(event.data);
      this.handler.handler(this.data.resource, this.data.data);
    };
  }

  /**
   * Desactive le handler
   * @param debug
   */
  public disableHandler(debug = false): void {
    this.useHandler = false;
    this.defaultAction(debug);
  }

  /**
   * Action par defaut de la socket à la reception d'un message
   * @param debug
   */
  private defaultAction(debug?: boolean): void {
    this.socket.onmessage = (event) => {
      if (debug) {
        console.log('Web Socket Message: ', event.data);
      }
      this.data = event.data;
    };
  }

}
