import {Injectable} from '@angular/core';
import {Player} from '../../model/Player';

@Injectable({
  providedIn: 'root'
})
export class DataService {

  /**
   * L'id de l'utilisateur
   */
  private id: number;

  /**
   * L'utilisateur à t'il le role de maitre du jeu (game master)
   */
  private gm: boolean;

  /**
   * Le pseudo de l'utilisateur
   */
  private pseudo: string;

  public players: Player[];

  /**
   * Données pour l'application
   */
  public data: any;

  public setBaseValue(id: number, gm: boolean, pseudo: string) {
    this.id = id;
    this.gm = gm;
    this.pseudo = pseudo;
  }

  public baseValueIsSet(): boolean {
    return this.pseudo !== undefined;
  }

  public getPlayer(pseudo: string): Player {
    if (this.players === undefined) {
      const player = new Player(pseudo);
      this.players = [player];
      return player;
    } else {
      let find = false;
      let pos = 0;
      this.players.forEach((elt, index) => {
        if (elt.getPseudo() === pseudo) {
          find = true;
          pos = index;
        }
      });
      if (find) {
        return this.players[pos];
      } else {
        const player = new Player(pseudo);
        this.players.push(player);
        return player;
      }
    }
  };

  public clearAnswers(): void {
    if (this.players !== undefined) {
      this.players.forEach(elt => {
        elt.answer = '';
      });
    }
  }

  public getWinners(): Player[] {
    if (this.players === undefined) {
      return [];
    }
    let score;
    this.players.forEach(elt => {
      if (score === undefined) {
        score = elt.score;
      } else if (elt.score > score) {
        score = elt.score;
      }
    });
    const winners = [];
    this.players.forEach(elt => {
      if (elt.score === score) {
        winners.push(elt);
      }
    });
    return winners;
  }

  public getId(): number {
    return this.id;
  }

  public isGm(): boolean {
    return this.gm;
  }

  public getPseudo(): string {
    return this.pseudo;
  }

}
