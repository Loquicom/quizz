<?php

use Ratchet\Http\HttpServer;
use Ratchet\Server\IoServer;
use Ratchet\WebSocket\WsServer;

require 'vendor' . DIRECTORY_SEPARATOR . 'autoload.php';
require 'Handler.php';
require 'WebSocketHandler.php';

$port = 8080;
if ($argc > 1) {
    $port = $argv[1];
}

echo "Starting server on port $port\n";

$server = IoServer::factory(
    new HttpServer(
        new WsServer(
            new WebSocketHandler(
                new Handler()
            )
        )
    ),
    $port
);

$server->run();
